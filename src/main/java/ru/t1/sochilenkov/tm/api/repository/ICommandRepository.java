package ru.t1.sochilenkov.tm.api.repository;

import ru.t1.sochilenkov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
