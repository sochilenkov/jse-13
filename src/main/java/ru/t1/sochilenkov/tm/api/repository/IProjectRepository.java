package ru.t1.sochilenkov.tm.api.repository;

import ru.t1.sochilenkov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    int getSize();

    boolean existsById(String id);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
