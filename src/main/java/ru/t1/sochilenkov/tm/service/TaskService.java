package ru.t1.sochilenkov.tm.service;

import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.ITaskService;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task project) {
        if (project == null) return null;
        return taskRepository.add(project);
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getSize()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        return add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public List<Task> findAllByProjectId(final String id) {
        if (id == null || id.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(id);
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    @Override
    public boolean existsById(final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        return taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
